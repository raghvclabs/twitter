//
//  ViewController.swift
//  twitterShare
//
//  Created by Click Labs on 3/10/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
import Social
import TwitterKit

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
  var pickerController:UIImagePickerController = UIImagePickerController()
  
  @IBOutlet var imageView: UIImageView!
  //MARK :-  Twitter Share Button
  @IBAction func twittershare(sender: AnyObject) {
    if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
      var twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
      twitterSheet.setInitialText(initialMessage)
      twitterSheet.addImage(imageView.image)
      self.presentViewController(twitterSheet, animated: true, completion: nil)
    } else {
      let  loginButton = TWTRLogInButton(logInCompletion: {
        (session: TWTRSession!, error: NSError!) in
        if (session != nil) {
          println("Signed in as \(session.userName)")
          println(session.description)
          println( session.authToken)
          println(session.userID)
        } else {
          println("Error: \(error.localizedDescription)")
        }
      })/*
      var alert = UIAlertController(title:loginAlertTitle , message: loginAlertMessage, preferredStyle: UIAlertControllerStyle.Alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
      self.presentViewController(alert, animated: true, completion: nil)*/
      loginButton.frame = CGRectMake(self.view.frame.origin.x + 60, self.view.frame.origin.y + 50 , self.view.frame.size.width/1.5, 40)
      self.view.addSubview(loginButton)
    }
    imageView.image = nil
  }
  //
  @IBAction func chooseImage(sender: AnyObject) {
    pickerController.delegate = self
    pickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
    self.presentViewController(pickerController, animated: true, completion: nil)
  }
  //MARK :-  image Picker delegate function
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func supportedInterfaceOrientations() -> Int {
    return Int(UIInterfaceOrientationMask.Portrait.rawValue)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

