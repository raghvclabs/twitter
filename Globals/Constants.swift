//
//  Constants.swift
//  twitterShare
//
//  Created by Click Labs on 3/19/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
let initialMessage = "Share on Twitter"
let loginAlertMessage = "Please login to a Twitter account to share."
let loginAlertTitle = "Accounts"